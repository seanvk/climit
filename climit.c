#define _GNU_SOURCE
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#define MAX_LAT 16
#define MAX_DUR 60
int max_lat;
int start_flag;
int start_fd;

/* C state latency points for limiting deepest C state */
static int latency_points[MAX_LAT];

static void
print_usage (void)
{
  fprintf (stderr,
           "climit\n"
           "\t-l: limit cstate (0 .. n-1, where n is the max latency found)\n"
           "\t-d: duration minutes (60 max)\n"
           "\t-h: print this message\n");
}

/* assumption is that all logical cpus have the same C state exit latencies */

static void
read_latencies (void)
{
  FILE *file;
  char line[4096];
  DIR *dir;
  struct dirent *ent;

  dir = opendir ("/sys/devices/system/cpu/cpu0/cpuidle/");
  assert (dir != NULL);

  do
    {
      ent = readdir (dir);
      if (!ent)
        break;

      if (ent->d_name[0] == '.')
        continue;

      if (strstr (ent->d_name, "state") == NULL)
        continue;

      sprintf (line, "/sys/devices/system/cpu/cpu0/cpuidle/%s/latency",
               ent->d_name);
      file = fopen (line, "r");
      if (!file)
        continue;
      if (fgets (line, 4096, file) != NULL)
        {
          fclose (file);
          latency_points[max_lat++] = strtoull (line, NULL, 10);
        }
      else
        fprintf (stderr, "Error reading latency\n");
    }
  while (ent);

  closedir (dir);

  printf ("Found %i C state latencies\n", max_lat);
  assert (max_lat > 0);
}

static void
end_limit (int cookie)
{
  close (cookie);
}

static int
limit_cstate (int first_excluded_state)
{
  int maxlat;
  int fd;

  maxlat = latency_points[first_excluded_state];
  fd = open ("/dev/cpu_dma_latency", O_WRONLY);
  if (fd >= 0)
    {
      printf ("maxlat = %d\n", maxlat);
      if (write (fd, &maxlat, 4) < 0)
        fprintf (stderr, "Error writing latency\n");
    }
  else
    fprintf (stderr, "Error opening /dev/cpu_dma_latency\n");

  return fd;
}

void
sighandler (int sig)
{
  if (start_flag)
    end_limit (start_fd);
  exit (sig);
}

#define CPUCOUNT 2

int
main (int argc, char **argv)
{
  int opt;
  int start;
  int limit;
  int duration;

  start_flag = 0;
  limit = MAX_LAT;
  duration = MAX_DUR;

  signal (SIGABRT, &sighandler);
  signal (SIGTERM, &sighandler);
  signal (SIGINT, &sighandler);

  read_latencies ();

  if (argc < 4)
    {
      print_usage ();
      return -1;
    }

  while ((opt = getopt (argc, argv, "l:d:h")) != -1)
    {
      switch (opt)
        {
        case 'l':
          /* handle -l, get arg value from optarg */
          limit = atoi (optarg);
          if (limit >= max_lat)
            {
              print_usage ();
              return -1;
            }
          break;
        case 'd':
          /* handle -d, get arg value from optarg */
          duration = atoi (optarg);
          if ((duration <= 0) && (duration > 60))
            {
              print_usage ();
              return -1;
            }
          break;
        case 'h':
          /* print usage */
          print_usage ();
          return -1;
          break;
        case ':':
          /* missing option argument */
          print_usage ();
          return -1;
          break;
        case '?':
        default:
          /* invalid option */
          print_usage ();
          return -1;
          break;
        }
    }

  start_flag = 1;
  start = time (NULL);
  if ((start_fd = limit_cstate (limit)) < 0)
    return -1;
  printf ("Setting limit to %d, for a duration of %d min\n", limit, duration);
  while (time (NULL) - start < duration * 60)
    usleep (1000); // wait a wee bit here
  end_limit (start_fd);

  return EXIT_SUCCESS;
}
