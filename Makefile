all: climit 

CFLAGS += -O2 -Wall -W 


OBJS := climit.o

climit: $(OBJS)
	gcc -o climit $(OBJS)



%.o: %.c Makefile
	@echo "  CC  $<"
	@[ -x /usr/bin/cppcheck ] && /usr/bin/cppcheck -q $< || :
	@$(CC) $(CFLAGS) -c -o $@ $<


clean:
	rm  -f *~ climit *.o
	
